<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Buyer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ShopController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $repository)
    {
        $this->orderRepository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $saved = $this->orderRepository->save($request->all());

        return new Response([
            'result' => $saved ? 'success' : 'fail',
            'error' => $saved ? null : $this->orderRepository->getErrorMessage(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $order = $this->orderRepository->findById($id);

        return new Response($order);    // time is over...
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $saved = $this->orderRepository->save($request->all());

        return new Response([
            'result' => $saved ? 'success' : 'fail',
            'error' => $saved ? null : $this->orderRepository->getErrorMessage(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
