<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'buyers';

    protected $fillable = [
        'name',
        'surname',
        'country',
        'city',
        'addressLine',
        'phone',
        'orders',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'buyer', 'id');
    }
}
