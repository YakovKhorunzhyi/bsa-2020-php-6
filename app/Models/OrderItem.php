<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'order_items';

    protected $fillable = [
        'order',
        'product',
        'quantity',
        'price',
        'discount',
        'sum',
        'buyer'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
