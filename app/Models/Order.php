<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'orders';

    protected $fillable = [
        'date',
        'orderItems',
        'buyer',
    ];

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order', 'id');
    }

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }
}
