<?php

namespace App\Repositories;

use App\Models\Buyer;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\DB;


class OrderRepository implements IRepository
{
    private $errorMessage = '';

    public function findById(int $orderId)
    {
        $order = Order::find($orderId);

        if (!$orderId) {
            return null;
        }

        $items = OrderItem::where('order', $orderId)->get();
        $buyer = Buyer::where('order', $orderId)->get();

        return [
            'order' => $order->toArray(),
            'items' => $items->toArray(),
            'buyer' => $buyer->toArray(),
        ];
    }

    public function save(array $order): bool
    {
        $saved = false;

        try {
            DB::beginTransaction();
            if (array_key_exists('buyerId', $order)) {
                $order = new Order([
                    'buyer' => $order['buyerId'],
                ]);

                $order->save();
            } else {
                OrderItem::where('order', $order['orderId'])->delete();
                $order = Order::find($order['orderId']);
            }

            $orderItems = $order['orderItems'];

            foreach ($orderItems as $item) {
                $orderItem = new OrderItem($item);
                $orderItem->product = $order['productId'];
                $orderItem->quantity = $order['productQty'];
                $orderItem->discount = intval($order['productDiscount']) / 100;
                $orderItem->save();
            }

            DB::commit();

            $saved = true;
        } catch (\Throwable $ex) {
            DB::rollBack();
            $this->errorMessage = $ex->getMessage();
        }

        return $saved;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }
}
