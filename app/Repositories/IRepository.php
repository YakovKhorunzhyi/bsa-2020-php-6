<?php

namespace App\Repositories;

interface IRepository
{
    public function save(array $arr);
}
