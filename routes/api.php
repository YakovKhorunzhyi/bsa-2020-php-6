<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::apiResource('orders', 'ShopController');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::delete('hello/{name}', function ($name){
    return [
        'name' => $name,
        'status' => 'deleted'
    ];
});

//Route::get('products', 'ProductController@index');
//Route::get('products/{id}', 'ProductController@show');
//Route::post('products', 'ProductController@store');
Route::apiResource('products', 'ProductController');
Route::delete('products/by-seller/{sellerId}', 'ProductController@deleteBySeller');
