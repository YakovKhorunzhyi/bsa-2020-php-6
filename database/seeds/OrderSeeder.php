<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;


class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        factory(Order::class, 10)
            ->create()
            ->each(function (Order $order) {
                $order
                    ->orderItems()
                    ->saveMany(
                        factory(OrderItem::class, 10)
                            ->make([
                                'order' => null,
                            ])
                    );
            });
    }
}
