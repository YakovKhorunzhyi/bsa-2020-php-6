<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\OrderItem;
use App\Models\Product;

$factory->define(OrderItem::class, function (Faker $faker) {
    $discount = rand(20, 80) / 100;
    $price = $faker->numberBetween(20, 8000);
    $quantity = rand(1, 10);

    return [
        'product' => Product::find(rand(1, 19)),
        'quantity' => $quantity,
        'price' => $price,
        'discount' => $discount,
        'sum' => $price * $discount * $quantity,
    ];
});
