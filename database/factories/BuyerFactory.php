<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Buyer;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'country' => $faker->country,
        'city' => $faker->city,
        'addressLine' => $faker->address,
        'phone' => $faker->phoneNumber
    ];
});
