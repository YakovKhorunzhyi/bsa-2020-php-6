<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Order;
use App\Models\Buyer;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'date' => now(),
        'buyer' => function () {
            return factory(Buyer::class)->create()->id;
        },
    ];
});
